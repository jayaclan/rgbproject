//
//  ViewController.swift
// RGBPROJED
//
//  Created by Louis  Valen on 17/05/19.
//  Copyright © 2019 Louis  Valen. All rights reserved.
//

import UIKit
import AVFoundation
class ViewController: UIViewController {
   
    var SoundEffect:AVAudioPlayer?
    @IBOutlet weak var targetBox: UIView!
    
    @IBOutlet weak var ScoreLabel: UILabel!
    
    @IBOutlet weak var targetBox2: UIView!
    
    @IBOutlet weak var YellowBox: UIView!
    
    @IBOutlet weak var UnguBox: UIView!
    
    @IBOutlet weak var TimerLabel: UILabel!
    @IBOutlet weak var labelSucces: UILabel!
    
    @IBOutlet weak var SameColor: UIView!
    @IBOutlet weak var CyanBox: UIView!
    var merah = 0
    var hijau = 0
    var biru = 0
    var Score = 0
    
    var timer:Timer?
    var time = 60.001
    var milliseconds:Float = 20 * 1000// Start from zero
    
    @IBOutlet weak var Startbutton: UIButton!
    
    @IBOutlet weak var StaterView: UIView!
    @IBAction func StartButtonAction(_ sender: Any) {
        
        StartGameDidLoad()
        StaterView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StaterView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        
        // Do any additional setup after loading the view.
        let gestureYellowBox = UITapGestureRecognizer(target: self, action: #selector(funcYellowBox))
        let gestureCyanBox = UITapGestureRecognizer(target: self, action: #selector(funcCyanBox))
        let gestureUnguBox = UITapGestureRecognizer(target: self, action: #selector(funcUnguBox))
        
        let gestureSameColor = UITapGestureRecognizer(target: self, action: #selector(funcSameColor))
        YellowBox.addGestureRecognizer(gestureYellowBox)
        CyanBox.addGestureRecognizer(gestureCyanBox)
        UnguBox.addGestureRecognizer(gestureUnguBox)
        SameColor.addGestureRecognizer(gestureSameColor)
        UnguBox.layer.cornerRadius = 50
        YellowBox.layer.cornerRadius = 50
        CyanBox.layer.cornerRadius = 50
        SetColorBox()
       
    }
    
    
    func SetColorBox() {
        targetBox.backgroundColor = GenerateColorPertama()
        targetBox2.backgroundColor = GenerateColorKedua()
    }
    
    func GenerateColorPertama() -> UIColor {
        merah = 0
        hijau = 0
        biru = 0
        var ngacak = Int.random(in: 1...3)
        if ngacak == 1 {
             merah = 1
            return .red
           
        } else if ngacak == 2 {
            hijau = 1
            return .green
            
        }else if ngacak == 3 {
            biru = 1
          return .blue
            
        }else{
            return .white
        }
    }
    
    
    func GenerateColorKedua() -> UIColor {
        var ngacak = Int.random(in: 1...3)
        if ngacak == 1 {
            merah = 1
            return .red
            
        } else if ngacak == 2 {
            hijau = 1
            return .green
            
        }else if ngacak == 3 {
            biru = 1
            return .blue
            
        }else{
            return .white
        }
    }
    
    
    @objc func timerElapsed() {
        
        milliseconds -= 1
        
        // Convert to seconds
        let seconds = String(format: "%.2f", milliseconds/1000)
        
        // Set label
        TimerLabel.text = "\(seconds)"
        
        // When the timer has reached 600 seconds
        if milliseconds == 0 {
            
            // Stop the timer
            timer?.invalidate()
            TimerLabel.textColor = UIColor.red
            alertGameOver()
    
        }
    }
    func TimerStart()  {
        // Create timer
        milliseconds = 20 * 1000 // 20 detik
        timer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(timerElapsed), userInfo: nil, repeats: true)
        TimerLabel.textColor = UIColor.black
    }

    func BackHome()  {
        StaterView.isHidden = false
    }
    func alertGameOver() {
        
        let alert = UIAlertController(title: "Game Over", message: "Score : \(Score)", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Home", style: .default, handler: {
            action in self.BackHome()
        }
        ))
        
        alert.addAction(UIAlertAction(title: "Again", style: .default, handler: {
            action in self.StartGameDidLoad()
        }
        ))
        
        self.view?.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    func StartGameDidLoad() {
        
        let alert = UIAlertController(title: "Start the game", message: "wrong answer Minus your score", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Start", style: .default, handler: {
            action in self.TimerStart()
        }
        ))
        
        
        self.view?.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    @objc func funcSameColor(){
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            self.changeSameCollor()
        }) { (isFinished) in
        }
    }
    @objc func funcYellowBox(){
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            self.changeYellowBox()
        }) { (isFinished) in
        }
    }
    @objc func funcUnguBox(){
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            self.changeUnguBox()
        }) { (isFinished) in
        }
    }
    @objc func funcCyanBox(){
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            self.changeCyanBox()
        }) { (isFinished) in
        }
    }
    
    var cekTurn = true
    
    func changeYellowBox() {
        if merah == 1 && hijau == 1 {
           
            Score += 1
            SetColorBox()
            SoundCorrect()
        }else{
            if Score == 0 {
                Score = 0
            }else{
                Score -= 1
            }
            SoundWrong()
        }
        ScoreLabel.text = "\(Score)"
        heptic()
    }
    
    func changeSameCollor() {
        

        if (merah == 1 && hijau == 0 && biru == 0) || (merah == 0 && hijau == 1 && biru == 0)  || (merah == 0 && hijau == 0 && biru == 1){
            
            Score += 1
            SetColorBox()
            SoundCorrect()
        }else{
            SoundWrong()
            if Score == 0 {
                Score = 0
            }else{
                Score -= 1
            }
            
        }
        ScoreLabel.text = "\(Score)"
        heptic()
    }
    
    func changeUnguBox() {
        if merah == 1 && biru == 1 {
            SoundCorrect()
            Score += 1
            SetColorBox()
        }else{
            SoundWrong()
            if Score == 0 {
                Score = 0
            }else{
                Score -= 1
            }
          
        }
        ScoreLabel.text = "\(Score)"
        heptic()
    }
    
    func changeCyanBox() {
        if biru == 1 && hijau == 1 {
            SoundCorrect()
            Score += 1
            SetColorBox()
        }else{
            SoundWrong()
            if Score == 0 {
                Score = 0
            }else{
                Score -= 1
            }
            
        }
        ScoreLabel.text = "\(Score)"
        heptic()
        
    }
    
    
    func heptic()  {
        let gen = UINotificationFeedbackGenerator()
        gen.notificationOccurred(.success)
        let gens = UIImpactFeedbackGenerator(style: .heavy)
        gens.prepare()
        gens.impactOccurred()
    }
    
    func SoundCorrect() {
        let bundlePath = Bundle.main.path(forResource: "Bener", ofType: "mp3")
        let soundURL = URL(fileURLWithPath: bundlePath!)
        do {
            // Create audo player object
            SoundEffect = try AVAudioPlayer(contentsOf: soundURL)
            
            // Play the sound
            SoundEffect?.play()
        }
        catch {
            // Couldn't create audio player object, log the error
            print("gak ada sound effect")
        }
    }
    
    func SoundWrong() {
        let bundlePath = Bundle.main.path(forResource: "Salah", ofType: "mp3")
        let soundURL = URL(fileURLWithPath: bundlePath!)
        do {
            // Create audo player object
            SoundEffect = try AVAudioPlayer(contentsOf: soundURL)
            
            // Play the sound
            SoundEffect?.play()
        }
        catch {
            // Couldn't create audio player object, log the error
           print("gak ada sound effect")
        }
    }
    
}

